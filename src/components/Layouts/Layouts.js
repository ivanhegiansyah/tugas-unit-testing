import React from 'react';
import PropTypes from 'prop-types';
import SideNav from '../elements/SideNav';
import Header from '../elements/Header/Header';

export default function Layouts({ children }) {

  return (
    <div className="flex h-screen overflow-hidden">
      <SideNav />
      <div className="flex flex-col w-full">
        <Header />
        <div className="p-8 h-full">
          {children}
        </div>
      </div>
    </div>

  );
}


Layouts.propTypes = {
  children: PropTypes.object.isRequired,
};

import React from 'react';
import Button from '../Button';
import './Form.css';
import PropTypes from 'prop-types';

export default function Form({ onSubmitHandler, items }) {
  return (
    <form onSubmit={onSubmitHandler}>
      {items}
      <div className="form__action">
        <Button dest="/Products" style="btn--secondary">Cancle</Button>
        <Button dest={null} style="btn--custom" type="submit">Save</Button>
      </div>
    </form>
  );
}

Form.propTypes = {
  items: PropTypes.array.isRequired,
  onSubmitHandler: PropTypes.func.isRequired,
};

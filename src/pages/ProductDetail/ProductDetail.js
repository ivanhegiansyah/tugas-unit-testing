import React from 'react';
import { useParams } from 'react-router-dom';

export default function ProductDetail() {
  const { id } = useParams();
  return (
    <main className="p-8 bg-white h-full">
      <h1 className="text-2xl text-black">{ id }</h1>
    </main>
  );
}

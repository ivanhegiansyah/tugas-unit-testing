import React, { lazy, Suspense } from 'react';
import LazyFallback from '../components/elements/LazyFallback';

const Suspensed = (Element) => function suspense(props) {
  return (
    <Suspense fallback={<LazyFallback />}>
      <Element {...props} />
    </Suspense>
  );
};

export const Main = Suspensed(lazy(() => import('./Main')));
export const Belajar = Suspensed(lazy(() => import('./Belajar')));
export const Products = Suspensed(lazy(() => import('./Products')));
export const Users = Suspensed(lazy(() => import('./Users')));
export const ProductDetail = Suspensed(lazy(() => import('./ProductDetail')));
export const NewProduct = Suspensed(lazy(() => import('./NewProduct')));
export const Dashboard = Suspensed(lazy(() => import('./Dashboard')));

import React, { useState, useEffect } from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import Products from '../Products';
import { useLocation } from 'react-router-dom';

describe('src/pages/Products', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Products />);
    expect(tree).toMatchSnapshot();
  });

  test('test state is not null in setItemsData', () => {
    useLocation.mockImplementationOnce(() => {
      return {
        state: {
          id: '',
          name: '',
          price: 0,
          image: '',
          isDeleted: false,
          category: '',
          expiryDate: '',
        },
      };
    });

    const items = [];
    const setItems = jest.fn();
    useState.mockImplementationOnce(() => [items, setItems]);
    useEffect.mockImplementationOnce((f) => f());

    Products();
    expect(setItems).toHaveBeenCalledTimes(1);
  });

  test('test onDelete', () => {
    let dummy = [
      {
        id: '1',
        name: 'p1',
        price: 100,
        image: '1',
        description: '1',
        isDeleted: false,
        category: 'Ready',
        expiryDate: '2023-01-12',
      },
      {
        id: '2',
        name: 'p2',
        price: 200,
        image: '2',
        description: '2',
        isDeleted: false,
        category: 'Pre-Order',
        expiryDate: '2023-01-13',
      },
    ];

    const result = Products();
    const child = result.props.children;
    const table = child[2].props;
    const rowsDatas = table.rows;
    const fireRowsDatas = rowsDatas(dummy);
    const div = fireRowsDatas.props.children;
    const td = div.props.children[4];
    const modal = td.props.children;
    modal.props.onDelete('1');
    // expect(dummy[0].isDeleted).toBe(true);
  });

  test('test change opacity', () => {
    const dummyDate = {
      expiryDate: '2021-01-01',
    };

    const result = Products();
    const table = result.props.children[2];
    const tr = table.props.rows(dummyDate, 2);
    const className = tr.props.children.props.className;
    expect(className).toBe('p-2 opacity-40');
  });
});
